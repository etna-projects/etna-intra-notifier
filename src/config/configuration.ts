import Joi from 'joi';

export const envValidationSchema = Joi.object({
  PORT: Joi.number().default(3000),
  HOSTNAME: Joi.string().default('0.0.0.0'),
  CRON_SCHEDULE: Joi.string().default('*/15 * * * *'),
  NODE_ENV: Joi.string()
    .valid('development', 'production', 'test', 'provision')
    .default('development'),
  // Pushbullet
  USE_PUSBULLET: Joi.boolean().default(false),
  PUSHBULLET_ACCESS_TOKEN: Joi.string().when('USE_PUSBULLET', {
    is: true,
    then: Joi.string().required(),
    otherwise: Joi.string().default(''),
  }),
  PUSHBULLET_TARGET: Joi.string().when('USE_PUSBULLET', {
    is: true,
    then: Joi.string().required(),
    otherwise: Joi.string().default(''),
  }),
  // NodeMailer
  USE_NODEMAILER: Joi.boolean().default(false),
  NODEMAILER_HOST: Joi.string().when('USE_NODEMAILER', {
    is: true,
    then: Joi.string().required(),
    otherwise: Joi.string().default(''),
  }),
  NODEMAILER_PORT: Joi.number().when('USE_NODEMAILER', {
    is: true,
    then: Joi.number().required(),
    otherwise: Joi.number().default(587),
  }),
  NODEMAILER_USER: Joi.string().when('USE_NODEMAILER', {
    is: true,
    then: Joi.string().required(),
    otherwise: Joi.string().default(''),
  }),
  NODEMAILER_PASS: Joi.string().when('USE_NODEMAILER', {
    is: true,
    then: Joi.string().required(),
    otherwise: Joi.string().default(''),
  }),
  NODEMAILER_FROM: Joi.string().when('USE_NODEMAILER', {
    is: true,
    then: Joi.string().required(),
    otherwise: Joi.string().default(''),
  }),
  NODEMAILER_TO: Joi.string().when('USE_NODEMAILER', {
    is: true,
    then: Joi.string().required(),
    otherwise: Joi.string().default(''),
  }),
  NODEMAILER_SECURE: Joi.boolean().default(false),
  // Intra
  INTRA_LOGIN: Joi.string().required(),
  INTRA_PASSWORD: Joi.string().required(),
});

export default () => ({
  port: parseInt(process.env.PORT, 10),
  hostname: process.env.HOSTNAME,
  pushBullet: {
    enabled: process.env.USE_PUSBULLET,
    accessToken: process.env.PUSHBULLET_ACCESS_TOKEN,
    target: process.env.PUSHBULLET_TARGET,
  },
  nodeMailer: {
    enabled: process.env.USE_NODEMAILER,
    host: process.env.NODEMAILER_HOST,
    port: parseInt(process.env.NODEMAILER_PORT, 10),
    user: process.env.NODEMAILER_USER,
    pass: process.env.NODEMAILER_PASS,
    from: process.env.NODEMAILER_FROM,
    to: process.env.NODEMAILER_TO,
  },
  intra: {
    login: process.env.INTRA_LOGIN,
    password: process.env.INTRA_PASSWORD,
  },
});
