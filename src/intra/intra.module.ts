import { Module } from '@nestjs/common';
import { IntraController } from './intra.controller';
import { IntraService } from './intra.service';

@Module({
  controllers: [IntraController],
  providers: [IntraService],
})
export class IntraModule {}
