import { Module } from '@nestjs/common';
import configuration, { envValidationSchema } from './config/configuration';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { IntraModule } from './intra/intra.module';
import { PushbulletModule } from './pushbullet/pushbullet.module';
import { ConditionalModule, ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      validationSchema: envValidationSchema,
      isGlobal: true,
    }),
    EventEmitterModule.forRoot({
      delimiter: '.',
      global: true,
    }),
    IntraModule,
    ConditionalModule.registerWhen(PushbulletModule, 'USE_PUSBULLET'),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
