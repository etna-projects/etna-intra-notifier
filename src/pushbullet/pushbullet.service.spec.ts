import { Test, TestingModule } from '@nestjs/testing';
import { PushbulletService } from './pushbullet.service';

describe('PushbulletService', () => {
  let service: PushbulletService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PushbulletService],
    }).compile();

    service = module.get<PushbulletService>(PushbulletService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
