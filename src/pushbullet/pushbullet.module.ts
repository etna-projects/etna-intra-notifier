import { Module } from '@nestjs/common';
import { PushbulletController } from './pushbullet.controller';
import { PushbulletService } from './pushbullet.service';

@Module({
  controllers: [PushbulletController],
  providers: [PushbulletService],
})
export class PushbulletModule {}
