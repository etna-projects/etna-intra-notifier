import { Test, TestingModule } from '@nestjs/testing';
import { PushbulletController } from './pushbullet.controller';

describe('PushbulletController', () => {
  let controller: PushbulletController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PushbulletController],
    }).compile();

    controller = module.get<PushbulletController>(PushbulletController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
